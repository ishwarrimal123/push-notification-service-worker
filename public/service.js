self.addEventListener("message", onMessage);
self.addEventListener("activate", onActivate);
self.addEventListener('push', onPushReceived);

// urlB64ToUint8Array is a magic function that will encode the base64 public key
// to Array buffer which is needed by the subscription option
const urlB64ToUint8Array = base64String => {
    const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
    const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/')
    const rawData = atob(base64)
    const outputArray = new Uint8Array(rawData.length)
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
  }

// saveSubscription saves the subscription to the backend
const saveSubscription = async subscription => {
    const SERVER_URL = 'http://localhost:4000/save-subscription'
    const response = await fetch(SERVER_URL, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(subscription),
    })
    return response.json()
}

async function onActivate(){
    console.log("SW activated")

    //Subsribe to push message
    try {
        const applicationServerKey = urlB64ToUint8Array(
            'BA9WnaAzQM05CMe7QkDie9V_6XNbIoSsAU-wuIXxFsxbfUylfYnJXMt3hPLtvCNCa7TkiB5tOWrg45xCJSYRrLU'
          )
        const options = { applicationServerKey, userVisibleOnly: true }
        const subscription = await self.registration.pushManager.subscribe(options)
        const response = await saveSubscription(subscription)
        console.log(response)
      } catch (err) {
        console.log('Error', err)
      }
}

main().catch(console.error);


// ****************************

async function main() {
	await sendMessage('123');
}


// send message to client
async function sendMessage(msg) {
    var allClients = await clients.matchAll({ includeUncontrolled: true, });
    return Promise.all(
        allClients.map(function sendTo(client) {
            var chan = new MessageChannel();
            chan.port1.onmessage = onMessage;
            return client.postMessage(msg, [chan.port2]);
        })
    );
}


// receive handler from client
async function onMessage(evt) {
    console.log('message at sw from client ', evt.data)
    // Make api call here and send it to client
    // await sendMessage('helloo')
}


// listen for push notification from backend
async function onPushReceived(event) {
    if (event.data) {
      console.log('Push event!! ', event.data.text())
    } else {
      console.log('Push event but no data')
    }
  }
