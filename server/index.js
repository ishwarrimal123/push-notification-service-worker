const express = require('express') 
const cors = require('cors') 
const webpush = require('web-push')
const bodyParser = require('body-parser')
const app = express() 
app.use(cors()) 
app.use(bodyParser.json()) 
const port = 4000 
app.get('/', (req, res) => res.send('Hello World!')) 

const subscriptionObj = { subscription: null } 

const updateSubscriptionObj = async subscription => {
    // store this in actual db later
  subscriptionObj.subscription = subscription
}
// The new /save-subscription endpoint
app.post('/save-subscription', async (req, res) => {
    debugger
  const subscription = req.body
  await updateSubscriptionObj(subscription) //Method to save the subscription to Database
  res.json({ message: 'success' })
})

const vapidKeys = {
    publicKey:
      'BA9WnaAzQM05CMe7QkDie9V_6XNbIoSsAU-wuIXxFsxbfUylfYnJXMt3hPLtvCNCa7TkiB5tOWrg45xCJSYRrLU',
    privateKey: 'OG4RxgtMV38jbvacRwCiSWPKMD2VO_aCVGWLsOXvans',
}


webpush.setVapidDetails(
    'mailto:ishwar.rimal@gmail.com',
    vapidKeys.publicKey,
    vapidKeys.privateKey
)

  //function to send the notification to the subscribed device
const sendNotification = (subscription, dataToSend='') => {
    webpush.sendNotification(subscription, dataToSend)
}

  //route to test send notification
app.get('/send-notification', (req, res) => {
    const subscription = subscriptionObj.subscription //get subscription from your databse here.
    const message = 'Hello World'
    sendNotification(subscription, message)
    res.json({ message: 'message sent' })
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))