let svcworker;
let svcpermission;

export function register(config) {
  if ((process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'development') && 'serviceWorker' in navigator) {

    window.addEventListener('load', async () => {
      const swUrl = `${process.env.PUBLIC_URL}/service.js`;
      const swRegistration = await registerValidSW(swUrl, config);
      svcworker = swRegistration.installing || swRegistration.waiting || swRegistration.active;
      navigator.serviceWorker.addEventListener("message", onMessage, false);
      sendMessage('From Client')
      console.log(Notification.permission)
      if(!svcpermission){
        await getNotificationPermission()
      }
      showLocalNotification()
    });
  }
}

//register new sw
const registerValidSW = async (swUrl) => {
  const swRegistration = await navigator.serviceWorker.register(swUrl)
  return swRegistration
}

const getNotificationPermission = async () => {
  const svcpermission = await window.Notification.requestPermission();
  // value of permission can be 'granted', 'default', 'denied'
  // granted: user has accepted the request
  // default: user has dismissed the notification permission popup by clicking on x
  // denied: user has denied the request.
  if(svcpermission !== 'granted'){
      throw new Error('Permission not granted for Notification');
  }
}

const showLocalNotification = () => {
  navigator.serviceWorker.ready.then(function(registration) {
    registration.showNotification('Vibration Sample', {
      body: 'Buzz! Buzz!',
      icon: '../images/touch/chrome-touch-icon-192x192.png',
      vibrate: [200, 100, 200, 100, 200, 100, 200],
      tag: 'vibration-sample'
    });
  });
}

const onMessage = (evt) => {
  console.log('received from SW ', evt.data)
  sendMessage('sending received msg to svcworker form client', evt.ports && evt.ports[0])
}


const sendMessage = (msg, target) => {
  if(target){
    target.postMessage(msg)
  }else if(svcworker){
    svcworker.postMessage(msg)
  }else{
    navigator.serviceWorker.postMessage(msg)
  }
}



export function unregister() {
  console.log(process.env)
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    });
  }
}
